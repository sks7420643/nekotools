package kame.nekotools.inventory.invswitch.repositories.models;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public abstract class PlayerData {
    protected final InventoryData inventory;
    protected final StatusData status;

    protected PlayerData(@NotNull InventoryData inventory, @NotNull StatusData status) {
        this.inventory = inventory;
        this.status = status;
    }

    public void setPlayerData(@NotNull Player player) {
        player.getInventory().setContents(inventory.items());
        player.setItemOnCursor(inventory.cursor());
        player.setHealth(status.health());
        player.setFoodLevel(status.food());
        player.setSaturation(status.saturation());
        player.setLevel(status.level());
        player.setExp(status.exp());
        player.getActivePotionEffects().stream().map(PotionEffect::getType).forEach(player::removePotionEffect);
        player.addPotionEffects(Arrays.asList(status.effects()));
    }
}

