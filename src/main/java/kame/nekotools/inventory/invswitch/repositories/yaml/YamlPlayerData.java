package kame.nekotools.inventory.invswitch.repositories.yaml;

import kame.nekotools.inventory.invswitch.repositories.models.InventoryData;
import kame.nekotools.inventory.invswitch.repositories.models.PlayerData;
import kame.nekotools.inventory.invswitch.repositories.models.StatusData;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YamlPlayerData extends PlayerData implements ConfigurationSerializable {

    YamlPlayerData() {
        this(new InventoryData(), new StatusData());
    }

    public YamlPlayerData(@NotNull Player player) {
        this(new InventoryData(player), new StatusData(player));
    }

    private YamlPlayerData(@NotNull InventoryData inventory, @NotNull StatusData status) {
        super(inventory, status);
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        var map = new HashMap<String, Object>();
        map.put("items", Arrays.asList(inventory.items()));
        map.put("cursor", inventory.cursor());
        map.put("health", status.health());
        map.put("food", status.food());
        map.put("saturation", status.saturation());
        map.put("level", status.level());
        map.put("exp", status.exp());
        map.put("effects", Arrays.asList(status.effects()));
        return map;
    }

    public static YamlPlayerData deserialize(Map<String, Object> data) {
        var items = data.get("items") instanceof List<?> l ? l.stream()
                .map(ItemStack.class::cast).toArray(ItemStack[]::new) : null;
        var cursor = (ItemStack)data.get("cursor");
        var health = (double)data.get("health");
        var food = (int)data.get("food");
        var satu = (double)data.get("saturation");
        var level = (int)data.get("level");
        var exp = (double)data.get("exp");
        var effects = data.get("effects") instanceof List<?> l ? l.stream()
                .map(PotionEffect.class::cast).toArray(PotionEffect[]::new) : null;
        var inventory = new InventoryData(items, cursor);
        var status = new StatusData(health, food, (float)satu, level, (float)exp, effects);
        return new YamlPlayerData(inventory, status);
    }
}
