package kame.nekotools.inventory.itemfix.gui;

import kame.nekocore.inventory.ActionInventory;
import kame.nekocore.inventory.ActionItemStack;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class ItemFixEnchantment {
    public static void openGui(Player player, ItemStack item) {
        var inv = new ActionInventory("ItemFix Enchantment Editor", Enchantment.values().length + 9);
        var items = Arrays.stream(Enchantment.values()).map(x -> create(player, item, x)).toArray(ItemStack[]::new);
        for (int i = 0; i < items.length; i++) {
            inv.setItem(i, items[i]);
        }
        var back = new ActionItemStack(Material.BARRIER, "§4§l戻る");
        back.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
        back.addClickEvent(x -> ItemFix.openGui(player, item));
        inv.setItem(inv.getSize() - 1, back);
        inv.openInventory(player);
    }

    private static ActionItemStack create(Player player, ItemStack item, Enchantment enc) {
        var hasEnc = item.getEnchantments().containsKey(enc);
        var level = hasEnc ? item.getEnchantmentLevel(enc) : 0;
        var action = new ActionItemStack(hasEnc ? Material.ENCHANTED_BOOK : Material.BOOK);
        //action.setLocalizedName("enchantment." + enc.getKey().getNamespace() + "." + enc.getKey().getKey());
        action.setDisplayName("§bid§6:§f" + enc.getKey() + ", §blv§6:§a" + item.getEnchantments().get(enc));
        action.setLore("§f左クリック§6:§fレベル§aUP", "§f右クリック§6:§fレベル§cDOWN", "§fドロップ§6:§bリセット");
        action.addUnsafeEnchantment(enc, level);
        action.addClickEvent(x -> {
            switch (x.getClick()) {
                case LEFT, SHIFT_LEFT -> {
                    item.addUnsafeEnchantment(enc, level + 1);
                    player.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 1 + (level + 1) / 10f);
                }
                case RIGHT, SHIFT_RIGHT -> {
                    item.addUnsafeEnchantment(enc, Math.max(level - 1, 0));
                    player.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 1 + (level - 1) / 10f);
                }
                case DROP, CONTROL_DROP -> {
                    item.removeEnchantment(enc);
                    player.playSound(player, Sound.BLOCK_GRINDSTONE_USE, 10, 2);
                }
            }
            openGui(player, item);
        });
        action.removeHideFlags(ItemFlag.HIDE_ENCHANTS);
        return action;
    }
}
