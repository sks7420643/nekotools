package kame.nekotools.inventory.itemfix.gui;

import kame.nekotools.NekoTools;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.function.Consumer;

class EventListener implements Listener {

    private static final Map<UUID, Registry> editing = new HashMap<>();

    static {
        Bukkit.getPluginManager().registerEvents(new EventListener(), JavaPlugin.getPlugin(NekoTools.class));
        Bukkit.getScheduler().runTaskTimer(JavaPlugin.getPlugin(NekoTools.class), EventListener::onTimer, 20, 20);
    }

    private static void onTimer() {
        editing.keySet().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).forEach(EventListener::sendCancelMessage);
    }

    private static void sendCancelMessage(Player player) {
        var message = "[itemfix] §dスニーク(SHIFTキー)で編集をキャンセルします。";
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
    }

    static void registerInternal(Player player, Consumer<String> written, Runnable finished) {
        editing.put(player.getUniqueId(), new Registry(written, finished));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onChat(AsyncPlayerChatEvent event) {
        var player = event.getPlayer();
        var item = editing.remove(player.getUniqueId());
        if (item != null) {
            event.setCancelled(true);
            player.playSound(player, Sound.BLOCK_ANVIL_USE, 10, 2);
            Bukkit.getScheduler().runTask(JavaPlugin.getPlugin(NekoTools.class), () -> item.accept(event.getMessage()));
        }
    }

    @EventHandler
    private void onSwap(PlayerSwapHandItemsEvent event) {
        event.setCancelled(cancel(event.getPlayer()) || event.isCancelled());
    }

    @EventHandler
    private void onSneak(PlayerToggleSneakEvent event) {
        event.setCancelled(cancel(event.getPlayer()) || event.isCancelled());
    }

    @EventHandler
    private void onDrop(PlayerDropItemEvent event) {
        event.setCancelled(cancel(event.getPlayer()) || event.isCancelled());
    }

    @EventHandler
    private void onClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player player) {
            event.setCancelled(cancel(player) || event.isCancelled());
        }
    }

    private boolean cancel(Player player) {
        var edit = Optional.ofNullable(editing.remove(player.getUniqueId()));
        edit.map(Registry::finished).ifPresent(Runnable::run);
        edit.ifPresent(x -> player.playSound(player, Sound.BLOCK_LAVA_POP, 10, 2));
        return edit.isPresent();
    }

    private record Registry(Consumer<String> written, Runnable finished) {
        void accept(String message) {
            written.accept(message);
            finished.run();
        }
    }
}
