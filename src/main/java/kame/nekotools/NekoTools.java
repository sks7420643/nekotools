package kame.nekotools;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPIBukkitConfig;
import dev.jorel.commandapi.RegisteredCommand;
import kame.nekotools.commands.NekoToolsCommand;
import kame.nekotools.update.PluginUpdater;
import kame.repositories.DependencyFinder;
import org.bukkit.Bukkit;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.*;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

public final class NekoTools extends JavaPlugin {

    private final Logger logger = getLogger();

    private final List<RegisteredCommand> commands = new ArrayList<>();

    private final Queue<Runnable> queue = new ArrayDeque<>();

    public static NekoTools getInstance() {
        return NekoTools.getPlugin(NekoTools.class);
    }

    @Override
    public void onLoad() {
        try {
            CommandAPI.onLoad(new CommandAPIBukkitConfig(this).silentLogs(true));
            var name = getClass().getPackage().getName() + ".commands.";
            var before = new ArrayList<>(CommandAPI.getRegisteredCommands());
            try (var jar = new JarFile(getFile())) {
                jar.stream()
                        .map(ZipEntry::getName)
                        .filter(x -> x.endsWith("Command.class"))
                        .map(x -> x.replace('/', '.'))
                        .map(x -> x.substring(0, x.length() - 6))
                        .filter(x -> x.startsWith(name))
                        .forEach(this::loadClass);
            }
            commands.addAll(CommandAPI.getRegisteredCommands());
            commands.removeIf(before::contains);
            NekoToolsCommand.commands.addAll(commands);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void addRunOnEnablingQueue(Runnable run) {
        queue.add(run);
    }

    private void loadClass(String cls) {
        try {
            Class.forName(cls);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        CommandAPI.onEnable();
        if (!Bukkit.getPluginManager().isPluginEnabled("NekoCore")) {
            try {
                downloadDependency();
            } catch (IOException | InvalidPluginException | InvalidDescriptionException e) {
                throw new RuntimeException(e);
            }
            return;
        }
        queue.forEach(Runnable::run);
    }

    private void downloadDependency() throws IOException, InvalidPluginException, InvalidDescriptionException {
        logger.warning("NekoCore is not found.");
        var group = DependencyFinder.getGroupPackageUrl(54021868);
        var latest = DependencyFinder.getLatestVersion(new URL(group), "kame/NekoCore");
        if (latest.isPresent()) {
            var path = Path.of(getFile().getParent(), "NekoCore.jar");
            logger.info("Downloading... %s".formatted(PluginUpdater.getDownloadUrl(latest.get())));
            PluginUpdater.getDownloadUrl(latest.get());
            logger.info("Download completed!");
            var plugin = Bukkit.getPluginManager().loadPlugin(path.toFile());
            Bukkit.getPluginManager().enablePlugin(Objects.requireNonNull(plugin));
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        commands.stream().map(RegisteredCommand::commandName).forEach(CommandAPI::unregister);
        commands.clear();
        CommandAPI.onDisable();
    }
}
