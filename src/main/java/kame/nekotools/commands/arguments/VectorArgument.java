package kame.nekotools.commands.arguments;

import com.mojang.brigadier.context.CommandContext;
import dev.jorel.commandapi.CommandAPIBukkit;
import dev.jorel.commandapi.arguments.CommandAPIArgumentType;
import dev.jorel.commandapi.arguments.SafeOverrideableArgument;
import dev.jorel.commandapi.executors.CommandArguments;
import org.bukkit.util.Vector;

public class VectorArgument extends SafeOverrideableArgument<Vector, Vector> {
    public VectorArgument(String nodeName) {
        super(nodeName, CommandAPIBukkit.get()._ArgumentVec3(true), pos -> 0 + " " + 0 + " " + 0);
    }

    @Override
    public Class<Vector> getPrimitiveType() {
        return Vector.class;
    }

    @Override
    public CommandAPIArgumentType getArgumentType() {
        return CommandAPIArgumentType.LOCATION;
    }

    @Override
    public <Source> Vector parseArgument(CommandContext<Source> cmdCtx, String key, CommandArguments previousArgs) {
        var node = cmdCtx.getNodes().stream()
                .filter(x -> x.getNode().getName().equals(key))
                .iterator().next().getRange();
        var data = node.get(cmdCtx.getInput());
        var args = data.split(" ");
        return new Vector(Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]));
    }
}
