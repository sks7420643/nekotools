package kame.nekotools.commands.arguments;

import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import dev.jorel.commandapi.CommandAPIBukkit;
import dev.jorel.commandapi.arguments.CommandAPIArgumentType;
import dev.jorel.commandapi.arguments.SafeOverrideableArgument;
import dev.jorel.commandapi.executors.CommandArguments;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class MaterialArgument extends SafeOverrideableArgument<Material, Material> {

    public MaterialArgument(String nodeName) {
        super(nodeName, CommandAPIBukkit.get()._ArgumentItemStack(), x -> CommandAPIBukkit.get().convert(new ItemStack(x)));
    }

    @Override
    public Class<Material> getPrimitiveType() {
        return Material.class;
    }

    @Override
    public CommandAPIArgumentType getArgumentType() {
        return CommandAPIArgumentType.ITEMSTACK;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <Source> Material parseArgument(CommandContext<Source> cmdCtx, String key, CommandArguments previousArgs) throws CommandSyntaxException {
        return CommandAPIBukkit.get().getItemStack((CommandContext<Object>)cmdCtx, key).getType();
    }
}
