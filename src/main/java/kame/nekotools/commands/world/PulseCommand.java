package kame.nekotools.commands.world;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPIBukkit;
import dev.jorel.commandapi.arguments.IntegerRangeArgument;
import dev.jorel.commandapi.arguments.TimeArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.wrappers.IntegerRange;
import kame.nekocore.utils.NumberUtils;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class PulseCommand implements CommandExecutor, TabCompleter, Listener {

    private static final String description =
            "§6Description:§r ブロックから実行することによってクロック信号を出力します。";

    private static final IntegerRange default_value = new IntegerRange(0, 0);

    static {
        // clock cycle [range]
        Register.create("pulse", description, "nekotools.command.clock")
                .withArguments(new TimeArgument("cycle"))
                .withArguments(new IntegerRangeArgument("ranges"))
                .executesCommandBlock(PulseCommand::pulse)
                .register();
        NekoTools.getInstance().addRunOnEnablingQueue(PulseCommand::registerCommand);
        try {
            var constructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);
            var pluginCommand = constructor.newInstance("pulse", NekoTools.getInstance());
            CommandAPIBukkit.get().getSimpleCommandMap().register("minecraft", pluginCommand);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void registerCommand() {
        var pulse = new PulseCommand();
        var command = Objects.requireNonNull(NekoTools.getInstance().getCommand("pulse"));
        command.setExecutor(pulse);
        command.setTabCompleter(pulse);
        Bukkit.getPluginManager().registerEvent(ServerCommandEvent.class, pulse, EventPriority.NORMAL, PulseCommand::execute, NekoTools.getInstance());

    }

    private static void execute(Listener listener, Event event) {
        if (event instanceof ServerCommandEvent serverCommand && serverCommand.getCommand().startsWith("pulse")) {
            if (serverCommand.getSender() instanceof BlockCommandSender sender) {
                serverCommand.setCancelled(!onCommand(sender, serverCommand.getCommand()));
            } else {
                serverCommand.getSender().sendMessage("§c[NekoTools] このコマンドはコマンドブロックから実行します。");
            }
        }
    }

    private static final Map<String, PulseArgumentCache> cache = new HashMap<>();

    private static boolean onCommand(BlockCommandSender sender, String command) {
        try {
            var pulse = cache.computeIfAbsent(command, PulseArgumentCache::new);
            for (var range : pulse.ranges) {
                if (pulseValue(sender.getBlock().getWorld().getGameTime(), pulse.cycle, range)) return true;
            }
        } catch (IndexOutOfBoundsException e) {
            sender.sendMessage("§c[NekoTools] コマンドの引数が完全ではありません。");
            sender.sendMessage("§c[NekoTools] /pulse <cycle> <ranges>...");
            Bukkit.getLogger().warning("CommandBlock[" + sender.getBlock().getLocation() + "] was thrown");
            e.printStackTrace();
        } catch (NumberFormatException e) {
            sender.sendMessage("§c[NekoTools] コマンドの引数が不正です。");
            sender.sendMessage("§c[NekoTools] /pulse <cycle> <ranges>...");
            Bukkit.getLogger().warning("CommandBlock[" + sender.getBlock().getLocation() + "] was thrown");
            e.printStackTrace();
        }
        return false;
    }

    private static int pulse(BlockCommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var cycle = args.getOrSupply("cycle", CommandArgs.thrown(Integer.class));
        var range = args.getOrSupply("ranges", CommandArgs.thrown(IntegerRange.class));
        if (pulseValue(sender.getBlock().getWorld().getGameTime(), cycle, range)) {
            return 1;
        }
        throw CommandAPI.failWithString("off");
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        return true;
    }

    private static boolean pulseValue(long time, int cycle, IntegerRange range) {
        return range.isInRange((int)(time % cycle));
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 1) return List.of("4", "10", "20", "100");
        if (args.length >= 2) {
            var cycle = NumberUtils.parse(args[0], 1);
            return List.of("0", "..1", ".." + (cycle / 2 - 1), (cycle / 2) + "..", "0.." + (cycle - 1));
        }
        return List.of();
    }

    private record PulseArgumentCache(int cycle, IntegerRange[] ranges) {

        public PulseArgumentCache(String command) {
            this(command.split(" "));
        }

        public PulseArgumentCache(String[] args) {
            this(Integer.parseInt(args[1]), Stream.of(args).skip(2).map(NumberUtils::parseIntRange));
        }

        private PulseArgumentCache(int cycle, Stream<IntegerRange> ranges) {
            this(cycle, ranges.toArray(IntegerRange[]::new));
        }
    }
}
