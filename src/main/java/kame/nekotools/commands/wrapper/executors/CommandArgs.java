package kame.nekotools.commands.wrapper.executors;

import dev.jorel.commandapi.executors.CommandArguments;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CommandArgs {
    private final CommandArguments arguments;

    CommandArgs(CommandArguments arguments) {
        this.arguments = arguments;
    }

    public <T> T get(int index) {
        return arguments.getUnchecked(index);
    }

    public <T> T get(String name) {
        return arguments.getUnchecked(name);
    }

    public <T> T getOrSupply(String name, Supplier<T> supplier) {
        return arguments.argsMap().containsKey(name) ? arguments.getUnchecked(name) : supplier.get();
    }

    public <T> T getOrThrow(String name, Class<T> type) {
        return arguments.argsMap().containsKey(name) ? arguments.getUnchecked(name) : ThrowIfEmpty();
    }

    public <T> Collection<T> listOrThrow(String name, Class<T> type) {
        return arguments.argsMap().containsKey(name) ? arguments.getUnchecked(name) : ThrowIfEmpty();
    }

    public static Collection<Player> throwPlayers() {
        throw new NoSuchElementException();
    }

    public static Collection<Player> emptyPlayers() {
        return Collections.emptyList();
    }

    public static Collection<Entity> throwEntities() {
        throw new NoSuchElementException();
    }

    public static Collection<Entity> emptyEntities() {
        return Collections.emptyList();
    }

    public static <T> Supplier<T> thrown(Class<T> clazz) {
        return () -> Throw(new NoSuchElementException());
    }

    public static <T> Supplier<T> value(T value) {
        return () -> value;
    }


    public static <T> Supplier<Collection<T>> throwValues(Class<T> clazz) {
        return () -> Throw(new NoSuchElementException());
    }

    @SafeVarargs
    public static <T> Supplier<Collection<T>> values(T... value) {
        return () -> List.of(value);
    }

    public static <T> Supplier<Collection<T>> supply(Supplier<T> supplier) {
        return () -> Collections.singleton(supplier.get());
    }

    public static <T> Supplier<Collection<T>> empty(Class<T> clazz) {
        return Collections::emptyList;
    }

    public static Supplier<Predicate<ItemStack>> itemPredicate(Predicate<ItemStack> predicate) {
        return () -> predicate;
    }

    public <T> Optional<T> getOptional(String name) {
        return Optional.ofNullable(arguments.getUnchecked(name));
    }

    public <T> Optional<T> getOptional(String name, Class<T> clazz) {
        return Optional.ofNullable(arguments.get(name)).filter(clazz::isInstance).map(clazz::cast);
    }

    public <T> void ifPresent(String name, Class<T> clazz, Consumer<T> consumer) {
        getOptional(name, clazz).ifPresent(consumer);
    }

    private static <T extends RuntimeException, U> U Throw(T exception) throws T {
        throw exception;
    }

    private static <T> T ThrowIfEmpty() {
        throw new NoSuchElementException();
    }

}
