package kame.nekotools.commands.effect;

import dev.jorel.commandapi.arguments.LiteralArgument;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.entity.Player;

public class AsyncParticleBuilderCommand {

    private static final String description =
            "§6Description:§r 非同期処理でパーティクルエフェクトを発生させます。";

    static {
        Register.create("asyncparticle", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("builder"))
                .withArguments(new LiteralArgument("bezier"))
                .executesPlayer(AsyncParticleBuilderCommand::bezierBuilder)
                .register();

    }

    private static void bezierBuilder(Player sender, CommandArgs args) {

    }
}
