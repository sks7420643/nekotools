package kame.nekotools.commands;

import org.bukkit.*;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface CommandCompleter extends TabExecutor {

    default List<String> matchPlayers(String arg) {
        return matches(arg, Bukkit.getOnlinePlayers().stream().map(Player::getName));
    }

    default List<String> matchOfflinePlayers(String arg) {
        return matches(arg, Arrays.stream(Bukkit.getOfflinePlayers()).map(OfflinePlayer::getName));
    }

    default List<String> matchWorlds(String arg) {
        return matches(arg, Bukkit.getWorlds().stream().map(World::getName));
    }

    default List<String> matchMaterials(String arg) {
        return matchKeys(arg, Arrays.stream(Material.values()).map(Material::getKey));
    }

    default List<String> matchEntityTypes(String arg) {
        return matchKeys(arg, Arrays.stream(EntityType.values()).map(EntityType::getKey));
    }

    default List<String> matchKeys(String arg, Stream<NamespacedKey> stream) {
        return stream.filter(x -> isMatchKey(x, arg)).map(NamespacedKey::toString).sorted().collect(Collectors.toList());
    }

    default List<String> matches(String arg, String... args) {
        return matches(arg, Arrays.stream(args));
    }

    default List<String> matches(String arg, Stream<String> stream) {
        return stream.filter(x -> isMatch(x, arg)).collect(Collectors.toList());
    }

    default boolean isMatchKey(NamespacedKey base, String input) {
        return isMatch(base.toString(), input) || isMatch(base.getKey(), input);
    }

    default boolean isMatch(String base, String input) {
        return base.toLowerCase().startsWith(input.toLowerCase());
    }
}
